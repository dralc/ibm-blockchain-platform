/*
 * SPDX-License-Identifier: Apache-2.0
 */

import { Context, Contract } from 'fabric-contract-api';

export class MyContract extends Contract {

    public async instantiate(ctx: Context): Promise<any> {
        const greeting = { text: 'Instantiate was called!' };
        await ctx.stub.putState('GREETING', Buffer.from(JSON.stringify(greeting)));
    }

    public async transaction1(ctx: Context, arg1: string): Promise<any> {
        console.info('transaction1', arg1);
        const greeting = { text: arg1 };
        await ctx.stub.putState('GREETING', Buffer.from(JSON.stringify(greeting)));
        return JSON.stringify(greeting);
    }

    // Add a member along with their email, name, address, and number
    public async addMember(ctx: Context, email: string, name: string, address: string, phoneNumber: string) {
        const member = {
            address,
            email,
            name,
            number: phoneNumber,
        };
        await ctx.stub.putState(email, Buffer.from(JSON.stringify(member)));
        return JSON.stringify(member);
    }

    // look up data by key
    public async query(ctx: Context, key: string) {
        console.info('querying for key: ' + key );
        const returnAsBytes = await ctx.stub.getState(key);
        const result = JSON.parse(returnAsBytes.toString());
        return JSON.stringify(result);
    }

}
